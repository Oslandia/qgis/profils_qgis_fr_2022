#! python3  # noqa: E265

"""
    QGIS startup script to synchronize profiles configurations.
"""

# standard lib
import configparser
import json
import logging
import re
import shutil
import tempfile
from logging.handlers import TimedRotatingFileHandler
from os import getenv
from os.path import expandvars
from pathlib import Path
from sys import exit
from sys import platform as opersys
from typing import Union
from urllib.request import getproxies

# 3rd party
import requests

# PyQGIS
try:
    from qgis.core import QgsApplication
    from qgis.PyQt.QtCore import QStandardPaths

    IS_PYQGIS_LOADED: bool = True
except ImportError as err:
    logging.error(f"QGIS and its API are not available. Trace: {err}")
    IS_PYQGIS_LOADED: bool = False

# -- GLOBALS ---------------------------------------------------------------

# script version as str and tuple
__version__: str = "2.0.0"
__version_info__: tuple = tuple(
    [
        int(num) if num.isdigit() else num
        for num in __version__.replace("-", ".", 1).split(".")
    ]
)

# determine where to store log file depending on operating system
if (
    getenv("QGIS_CUSTOM_CONFIG_PATH")
    and Path(getenv("QGIS_CUSTOM_CONFIG_PATH")).exists()
):
    path_log_file: Path = Path(
        getenv("QGIS_CUSTOM_CONFIG_PATH") / "logs/qgis_startup.log"
    )
else:
    if IS_PYQGIS_LOADED:
        path_log_file: Path = (
            Path(QStandardPaths.standardLocations(QStandardPaths.AppDataLocation)[0])
            / "logs/qgis_startup.log"
        )
    else:
        if opersys == "win32":
            path_log_file: Path = Path(
                expandvars("%APPDATA%/QGIS/QGIS3/logs/qgis_startup.log")
            )
        elif opersys == "darwin":
            path_log_file: Path = (
                Path.home()
                / "Library/Application Support/QGIS/QGIS3/logs/qgis_startup.log"
            )
        else:
            path_log_file: Path = (
                Path.home() / ".local/share/QGIS/QGIS3/logs/qgis_startup.log"
            )

path_log_file.parent.mkdir(parents=True, exist_ok=True)
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[
        TimedRotatingFileHandler(
            filename=path_log_file,
            when="D",
            interval=1,
            backupCount=5,
        )
    ],
)
logging.info("===== QGIS startup script =====")

# retrieve local path where profiles are downloaded
if getenv("QDT_PROFILES_PATH") and Path(getenv("QDT_PROFILES_PATH")).is_dir():
    qdt_profiles_path: Path = Path(getenv("QDT_PROFILES_PATH"))
elif getenv("PYQGIS_STARTUP") and Path(getenv("PYQGIS_STARTUP")).exists():
    qdt_profiles_path = Path(getenv("PYQGIS_STARTUP")).parent
else:
    err_msg = "QDT_PROFILES_PATH nor PYQGIS_STARTUP not found or not pointing to an existing file."
    logging.error(err_msg)
    exit(err_msg)


if IS_PYQGIS_LOADED:
    path_qgis_profiles_dir = Path(QgsApplication.qgisSettingsDirPath()).parent
elif (
    getenv("QGIS_CUSTOM_CONFIG_PATH")
    and Path(getenv("QGIS_CUSTOM_CONFIG_PATH")).exists()
):
    path_qgis_profiles_dir = Path(getenv("QGIS_CUSTOM_CONFIG_PATH")) / "profiles"
else:
    path_qgis_profiles_dir = Path(expandvars("%APPDATA%/QGIS/QGIS3/profiles"))

logging.debug(f"QGIS profiles folder: {path_qgis_profiles_dir.resolve()}")


# -- FUNCTIONS -------------------------------------------------------------
def get_proxy_settings() -> Union[dict, None]:
    """Retrieves network proxy settings from operating system configuration or
    environment variables.

    :return Union[dict, None]: system proxy settings or None if no proxy is set
    """
    if getenv("HTTP_PROXY") or getenv("HTTPS_PROXY"):
        proxy_settings = {
            "http": getenv("HTTP_PROXY"),
            "https": getenv("HTTPS_PROXY"),
        }
        logging.debug(
            "Proxies settings found in environment vars (loaded from .env file): {}".format(
                proxy_settings
            )
        )
    elif getproxies():
        proxy_settings = getproxies()
        logging.debug("Proxies settings found in the OS: {}".format(proxy_settings))
    else:
        logging.debug("No proxy settings found in environment vars nor OS settings.")
        proxy_settings = None

    return proxy_settings


def getCDFilename(cd) -> str:
    """
    Get filename from content-disposition
    """
    if not cd:
        return None
    fname = re.findall("filename=(.+)", cd)
    if len(fname) == 0:
        return None
    return fname[0]


def getPluginVersion(path):
    metadataPath = path / "metadata.txt"
    if metadataPath.exists():
        for line in metadataPath.open():
            for result in re.findall("version=(.*)", line):
                if len(result) > 0:
                    return result
    else:
        logging.warning(str(metadataPath) + " not found")
        return 0


def getProfileVersion(path):
    profilePath = path / "profile.json"
    if profilePath.exists():
        profile = json.load(profilePath.open())
        return profile["version"]
    else:
        logging.warning(str(path) + "/profile.json not found")
        return 0


def ignore_ini(src):
    ignore = []
    for file in src.iterdir():
        if file.suffix == ".ini":
            logging.info("Add to ignore :" + str(file.name))
            ignore.append(file.name)
    return ignore


def overwriteDir(src, dest, ignore=None):
    if src.is_dir():
        # make sure that target dir exists
        dest.mkdir(parents=True, exist_ok=True)
        if ignore is not None:
            ignored = ignore(src)
        else:
            ignored = set()
        for f in src.iterdir():
            if f.name not in ignored:
                overwriteDir(f, dest / f.name, ignore)
    else:
        shutil.copyfile(src, dest)


def mergeInitFile(profileSrc, profileDest, iniFile):
    src = profileSrc / iniFile
    dest = profileDest / iniFile
    if src.exists():
        if dest.exists():
            configSrc = configparser.ConfigParser(
                interpolation=None, allow_no_value=True, strict=False
            )
            configSrc.optionxform = str
            with open(src, "r") as file:
                data = file.read()
            configSrc.read_string(data)
            configDest = configparser.ConfigParser(
                interpolation=None, allow_no_value=True, strict=False
            )
            configDest.optionxform = str
            configDest.read(dest)
            for section in configSrc:
                if section != "DEFAULT" and section not in configDest.sections():
                    configDest.add_section(section)
                for param in configSrc[section]:
                    configDest[section][param] = configSrc[section][param]
            with dest.open("w") as configfile:
                configDest.write(configfile)
        else:
            shutil.copyfile(src, dest)


def plugin_download_install(plugin_conf: dict, dest_path: Path) -> bool:
    """Download (or copy if local) and install plugin using the given configuration section.

    :param dict plugin_conf: plugin configuration section
    :param Path dest_path: where to install the plugin

    :return bool: True if the plugin has been downloaded and installed, False if something went wrong
    """
    if plugin_conf.get("location") == "local":
        pluginPath = Path(plugin_conf.get("url"))
        shutil.copytree(
            pluginPath, dest_path / plugin_conf.get("name"), dirs_exist_ok=True
        )
        logging.info(f"Local plugin copied from {pluginPath} to {dest_path}")
        return True
    elif plugin_conf.get("location") == "remote":
        # Downloading plugin from remote repository
        with tempfile.TemporaryDirectory(prefix="qgis_startup_") as tmp_dir:
            logging.info(
                f"== Download plugin from {plugin_conf.get('url')} into {tmp_dir}"
            )
            with requests.Session() as s:
                r = s.get(
                    url=plugin_conf.get("url"),
                    allow_redirects=True,
                    proxies=get_proxy_settings(),
                )

            if r.status_code != 200:
                logging.error(
                    f"Plugin download error: status code {r.status_code}, {r.reason}"
                )
                return False

            # Get plugin name
            if plugin_conf.get("url").endswith(".zip"):
                # auto-hosted repo: look into URL
                filename = plugin_conf.get("url").split("/")[-1]
            else:
                # official repo: get filename from response header
                filename = getCDFilename(r.headers.get("content-disposition"))

            if filename:
                tmp_plugin_path = Path(tmp_dir) / filename
                with tmp_plugin_path.open("wb") as f:
                    f.write(r.content)
                logging.info(f"Plugin downloaded to {tmp_plugin_path}")

                # Unzip plugin
                logging.info(f"== Unzip plugin {filename}")
                shutil.unpack_archive(tmp_plugin_path, dest_path)

                return True
            else:
                logging.error("Unable to retrieve plugin name: %s" % str(r.content))
            return False
    else:
        logging.error(f"Unknown plugin type: {plugin_conf.get('location')}")
        return False


def addPlugins(conf, profilePath):
    pathToPlugin = "python/plugins"
    # On iter sur les plugins du profil
    for plugin in conf["plugins"]:
        logging.info("plugin : " + plugin["name"])
        pluginPath = profilePath / pathToPlugin / plugin["name"]
        logging.info("path : " + str(pluginPath))
        # On check si le plugin existe
        if pluginPath.exists():
            # Si il existe, on test la version
            pluginVersion = getPluginVersion(pluginPath)
            # Si on a pas la meme version on le remplace par la version du dépôt
            if pluginVersion != plugin["version"]:
                logging.info("...... Delete and install plugin")
                shutil.rmtree(pluginPath)
                plugin_download_install(plugin, profilePath / pathToPlugin)
            else:
                logging.info("plugin up to date : " + pluginVersion)
        else:
            logging.info("...... Install plugin")
            plugin_download_install(plugin, profilePath / pathToPlugin)


# -- MAIN -------------------------------------------------------------------
def main():
    if qdt_profiles_path.exists() and qdt_profiles_path.is_dir():

        for profileqdt_profiles_path in [
            x for x in qdt_profiles_path.iterdir() if x.is_dir()
        ]:
            configFile = qdt_profiles_path / profileqdt_profiles_path / "profile.json"
            if configFile.exists():
                # le try catch permet d'éviter une erreur si l'utilisateur a les droits de lister le répertoire mais pas de lire les fichiers
                try:
                    # load profile.json
                    with configFile.open("r") as j:
                        profile = json.load(j)

                    profilePath: Path = path_qgis_profiles_dir / profile["name"]
                    profileUrl: Path = qdt_profiles_path / profileqdt_profiles_path

                    # On check si le profil existe
                    if profilePath.exists():
                        logging.debug(f"Existing profile: {profilePath}")
                        # Si il existe, on test la version
                        profileVersion = getProfileVersion(profilePath)
                        logging.debug(f"profile version : {profileVersion}")
                        # Si on a pas la meme version on overide les fichiers avec ceux du dépot...
                        if profileVersion != profile["version"]:
                            # On override le profile
                            overwriteDir(profileUrl, profilePath, ignore_ini)

                            # On merge les fichier .ini
                            qgisIniPath = Path("QGIS/QGIS3.ini")
                            qgisCustomisationIniPath = Path(
                                "QGIS/QGISCUSTOMIZATION3.ini"
                            )
                            mergeInitFile(profileUrl, profilePath, qgisIniPath)
                            mergeInitFile(
                                profileUrl, profilePath, qgisCustomisationIniPath
                            )

                            # On ajoute les plugins
                            addPlugins(profile, profilePath)
                        else:
                            logging.debug(
                                f"The installed profile '{profile.get('name')}' "
                                f"is up to date ({profileVersion})."
                            )
                    else:
                        # Si le profil n'existe pas on le copie depuis le dépot
                        shutil.copytree(profileUrl, profilePath)
                        # On ajoute les plugins
                        addPlugins(profile, profilePath)

                except:
                    logging.error("...... Profile access denied")

        logging.debug("==== END of startup script ====")

    logging.getLogger().removeHandler(logging.getLogger().handlers[0])


# ##################################
# ##### Stand alone program ########
# ##################################
if __name__ == "__main__":
    main()
