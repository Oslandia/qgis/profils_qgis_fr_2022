# Profils QGIS pour l'atelier des journées QGIS FR 2024

Cet entrepôt contient des profils QGIS, la logique de déploiement et le support de l'atelier.

## Déployer les profils

### Prérequis

- QGIS Deployment Toolbelt (aka QDT) >= 0.31 (voir [la page installation](https://guts.github.io/qgis-deployment-cli/usage/installation.html))
- connexion réseau sur :
  - gitlab.com
  - plugins.qgis.org

### Exécuter

#### Avec QDT installée sous forme de paquet Python (avec pip)

```sh
qdt -s https://gitlab.com/Oslandia/qgis/profils_qgis_fr/-/raw/main/qdt_scenarii/scenario.qdt.yml?ref_type=heads
```

#### Avec QDT téléchargée sous forme d'exécutable autoporteur (sans installation préalable requise)

Sur Linux :

```sh
chmod +x qdt.bin
./qdt.bin -s https://gitlab.com/Oslandia/qgis/profils_qgis_fr/-/raw/main/qdt_scenarii/scenario.qdt.yml?ref_type=heads
```

Sur Windows :

1. Renommer l'exécutable en `qdt.exe`
1. Exécuter :

  ```powershell
  ./qdt.exe -s https://gitlab.com/Oslandia/qgis/profils_qgis_fr/-/raw/main/qdt_scenarii/scenario.qdt.yml?ref_type=heads
  ```

----

## Contribuer aux profils

### Prérequis

- Python >= 3.10
- connexion réseau sur :
  - gitlab.com
  - plugins.qgis.org
  - pypi.org

### Environnement de travail

Création d'un environnement virtuel :

```sh
python -m venv .venv
# Sur Windows :
# py -3 -m venv .venv

. .venv/bin/activate
# Sur Windows :
# .venv/Scripts/activate
```

Installation des dépendances :

```sh
python -m pip install -U pip setuptools wheel
pip install -U -r requirements.txt
pre-commit install
```

### Exécuter QDT

Depuis l'environnement virtuel dans le projet cloné :

```sh
qgis-deployment-toolbelt -v -s qdt_scenarii/scenario.qdt.yml
```

### Vérifier la conformité des profils

> Vérification réalisée à l'aide des JSON Schema

```sh
check-jsonschema --schemafile https://raw.githubusercontent.com/qgis-deployment/qgis-deployment-toolbelt-cli/main/docs/schemas/profile/qgis_profile.json profiles/*/profile.json
```

### Vérifier la conformité des scénarios à l'aide des JSON Schemas

> Vérification réalisée à l'aide des JSON Schema

```sh
check-jsonschema --default-filetype yaml --base-uri https://raw.githubusercontent.com/Guts/qgis-deployment-cli/main/docs/schemas/scenario/ --schemafile https://raw.githubusercontent.com/Guts/qgis-deployment-cli/main/docs/schemas/scenario/schema.json qdt_scenarii/*.qdt.yml
```

----

## Tester le script `startup.py`

Il est possible de définir la variable d'environnement `PYQGIS_STARTUP` et d'exécuter QGIS.

Typiquement sur Ubuntu :

```bash
PYQGIS_STARTUP=$PWD/profiles/startup.py qgis --noversioncheck --profiles-path /tmp/test-qgis-startup
```

Sur Windows, avec PowerShell (en adaptant le chemin vers l'installation de QGIS) :

```powershell
$env:PYQGIS_STARTUP="$PWD/profiles/startup.py"
& $env:Programfiles/QGIS/3_22/bin/qgis-bin.exe
```

Ou via l'OSGeo4W Shell :

```cmd
set PYQGIS_STARTUP=%CD%\profiles\startup.py
qgis-ltr
```

### Fichiers logs

Le script de démarrage journalise les lancements de QGIS dans :
    - Linux : `~/.local/share/QGIS/QGIS3/logs`
    - Windows : `%USERPROFILE%\AppData\Local\QGIS\QGIS3\logs`
